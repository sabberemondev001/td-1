import { Backdrop, Box, Modal, Fade, Typography } from "@mui/material";
import StrawberryIcon from "../assets/svg/strawberry-logo.svg";

// --------------------------------------------------------

const Modal404 = ({ open, setOpen }) => (
	<Modal
		open={open}
		onClose={() => setOpen(false)}
		closeAfterTransition
		BackdropComponent={Backdrop}
		BackdropProps={{ timeout: 500 }}
		aria-labelledby="404-page-not-found"
		aria-describedby="404-page-not-found"
	>
		<Fade in={open}>
			<Box
				sx={{
					fontFamily: "'Poppins', sans-serif",
					color: "black.main",
					minWidth: 500,
					position: "absolute",
					top: "50%",
					left: "50%",
					transform: "translate(-50%, -50%)",
					boxShadow: 24,
					borderRadius: 3,
					textAlign: "center",
					bgcolor: "white.main",
					p: 5,
					pt: 3,
				}}
			>
				<StrawberryIcon />

				<Typography fontSize="28px" fontWeight={600}>
					Coming soon...
				</Typography>
				<Typography fontSize="20px" mt={1.5}>
					This page is still under development
				</Typography>
			</Box>
		</Fade>
	</Modal>
);

export default Modal404;
