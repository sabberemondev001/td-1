import { useState } from "react";
import { Box, Typography, IconButton, Snackbar, Alert } from "@mui/material";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import { elipsisMiddle } from "../utils/helpers";

// -----------------------------------------------------------------

const CopyToClipboard = ({ value, label }) => {
	const [copiedToastOpen, setCopiedToastOpen] = useState(false);

	return (
		<Box sx={{ display: "flex", gap: "12px", alignItems: "center" }}>
			<Typography sx={{ fontSize: 13, fontWeight: 300, lineHeight: "36px" }} noWrap>
				{elipsisMiddle(label || value)}
			</Typography>

			<IconButton
				size="small"
				onClick={() => {
					setCopiedToastOpen(true);
					navigator.clipboard.writeText(value);
				}}
			>
				<ContentCopyIcon fontSize="inherit" />
			</IconButton>

			<Snackbar
				open={copiedToastOpen}
				autoHideDuration={2000}
				onClose={() => setCopiedToastOpen(false)}
				anchorOrigin={{ vertical: "top", horizontal: "center" }}
			>
				<Alert
					elevation={6}
					variant="filled"
					severity="success"
					sx={{ width: "100%" }}
					onClose={() => setCopiedToastOpen(false)}
				>
					Copied to clipboard
				</Alert>
			</Snackbar>
		</Box>
	);
};

export default CopyToClipboard;
