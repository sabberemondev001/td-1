import { useState } from "react";
import { Box, Typography, Collapse } from "@mui/material";
import { ThemeUtils } from "../styles/theme/ThemeComponents";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { IconButton } from "@mui/material";

//

const NftGreenBox = ({ children, headerIcon, headerText, ...props }) => {
	const [opened, setOpened] = useState(true);
	const [isModeLight] = ThemeUtils();

	return (
		<Box border="1px solid" borderColor="secondary.main" borderRadius="15px" {...props}>
			<Box
				className="box-header flex-jus-bet"
				minHeight={70}
				borderBottom={opened ? "1px solid" : "none"}
				borderColor="secondary.main"
				borderRadius="15px"
				px="22px"
			>
				<Box display="flex" alignItems="center">
					<Box
						minWidth={42}
						display="flex"
						alignItems="center"
						sx={{ path: { ...(isModeLight ? { fill: "#131417" } : {}) } }}
					>
						{headerIcon}
					</Box>
					<Typography fontSize={{ xs: "20px", sm: "22px" }} fontWeight={700} noWrap>
						{headerText}
					</Typography>
				</Box>

				<IconButton color="secondary" onClick={() => setOpened((prev) => !prev)}>
					<KeyboardArrowDownIcon
						sx={{
							fontSize: { xs: "28px", sm: "34px" },
							color: "secondary.main",
							transition: "transform 0.3s",
							transform: opened ? "rotate(180deg)" : "none",
						}}
					/>
				</IconButton>
			</Box>

			<Collapse in={opened}>
				<Box px="26px" py="22px">
					{children}
				</Box>
			</Collapse>
		</Box>
	);
};

export default NftGreenBox;
