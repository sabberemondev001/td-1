import { useEffect, useRef, useState } from "react";
import { Box } from "@mui/material";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Tooltip, Filler } from "chart.js";
import ExternalTooltipHandler from "../utils/ChartCustomTooltip";

// --------------------------------------------------------------------------

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Tooltip, Filler);

//

const ChartComponent = ({ yData, xData, fontSize = 12.75, fontColor = "#71A9FF", canvasHeight }) => {
	const [chartData, setChartData] = useState({ datasets: [] });
	const chartRef = useRef(null);

	ChartJS.defaults.font.size = fontSize;
	ChartJS.defaults.color = fontColor;

	const createGradientBg = (context) => {
		const gradient = context?.createLinearGradient(0, 0, 0, 180);
		gradient?.addColorStop(0, "#5DD42570");
		gradient?.addColorStop(1, "#5DD42500");

		return gradient;
	};

	const options = {
		responsive: true,
		maintainAspectRatio: false,
		interaction: {
			mode: "index",
			intersect: false,
		},
		scales: {
			x: { grid: { display: false } },
			y: {
				grid: { display: false },
				ticks: { display: false },
			},
		},
		elements: {
			line: { tension: 0.4, borderWidth: 2 },
			point: { radius: 0, hoverRadius: 0 },
		},
		plugins: {
			tooltip: {
				enabled: false,
				position: "nearest",
				external: ExternalTooltipHandler,
			},
		},
	};

	useEffect(() => {
		const chart = chartRef.current;
		if (!chart) return;

		const data = {
			labels: xData,
			datasets: [
				{
					fill: true,
					data: yData,
					borderColor: "#50B720",
				},
			],
		};

		setChartData({
			...data,
			datasets: data.datasets.map((dataset) => ({
				...dataset,
				backgroundColor: createGradientBg(chartRef?.current?.ctx),
			})),
		});
	}, [xData, yData]);

	return (
		<Box
			sx={{
				height: canvasHeight || { xs: "180px", lg: "165px" },
				canvas: { width: "100% !important", height: "100% !important" },
			}}
		>
			<Line ref={chartRef} options={options} data={chartData} />
		</Box>
	);
};

export default ChartComponent;
