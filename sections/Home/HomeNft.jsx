import Image from "next/image";
import { Box, Container, Grid, Typography } from "@mui/material";

const HomeNft = () => (
	<Box component="section" id="NFT" sx={{ position: "relative" }}>
		<Container sx={{ py: 12 }}>
			<Box sx={{ position: "relative", flexGrow: 1, zIndex: 10 }}>
				<Grid
					container
					spacing={{ xs: 2, md: 3, lg: 8 }}
					columns={{ xs: 2, sm: 8, md: 12 }}
					sx={{ alignItems: "center" }}
				>
					<Grid
						data-aos="fade-right"
						data-aos-duration="1000"
						item
						xs={2}
						sm={4}
						md={6}
						sx={{
							img: {
								maxWidth: { md: "325px" },
								maxHeight: { xs: "340px", md: "none" },
								objectFit: "contain",
							},
						}}
					>
						<Image src="/images/root.svg" alt="" style={{ transform: "scale(1.65)", paddingbottom: "710px" }} layout="responsive"   width={250} height={150}  />
					</Grid>

					<Grid data-aos="fade-left" data-aos-duration="1000" item xs={2} sm={4} md={6}>
						<Box sx={{ textAlign: { xs: "center", md: "inherit" } }}>
							<Typography sx={{ fontSize: { xs: 32, md: 48 }, mb: 2 }} variant="h3">
								Alpha Radish{" "}
								<Box component="span" color="primary.main">
									NFT
								</Box>
							</Typography>
							<Typography
								sx={{
									fontSize: { xs: 16, md: 24 },
									fontWeight: 400,
									color: "black.100",
									mb: 3,
								}}
							>
								Our in-house Radish NFT is a collection of 10,000 digital radishes that will kickstart the
								RADISH platform. They are meant to be nurtured in a friendly community of like-minded rads in the
								beddings of RADIX DLT.
							</Typography>
						</Box>
					</Grid>
				</Grid>
			</Box>

			{/***
			 * ====================================
			 * >>> background absolute elements <<<
			 * ====================================
			 ***/}

			<Box
				className="float-right"
				sx={{
					position: "absolute",
					top: "40%",
					left: "12%",
					transform: "translate(-50%, -50%)",
					width: "200px",
					height: "200px",
					borderRadius: "50%",
					bgcolor: "primary.main",
					filter: "blur(100px)",
					zIndex: 1,
				}}
			/>

			<Box
				className="bounce-animate"
				sx={{
					position: "absolute",
					top: "70%",
					left: "20%",
					transform: "translate(-50%, -50%)",
					width: "160px",
					height: "160px",
					borderRadius: "50%",
					bgcolor: "secondary.main",
					filter: "blur(154px)",
					zIndex: 1,
				}}
			/>

			<Box
				className="float-left"
				sx={{
					position: "absolute",
					top: "5%",
					right: "-2%",
					transform: "translate(-50%, -50%)",
					width: "220px",
					height: "220px",
					borderRadius: "50%",
					bgcolor: "secondary.main",
					filter: "blur(120px)",
					zIndex: 1,
				}}
			/>
		</Container>
	</Box>
);

export default HomeNft;
