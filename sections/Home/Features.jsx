import { Box, Card, CardContent, Container, Grid, Typography } from "@mui/material";
import Feature1 from "../../assets/svg/feature-1.svg";
import Feature2 from "../../assets/svg/feature-2.svg";
import Feature3 from "../../assets/svg/feature-3.svg";

//

const features = [
	{
		id: 1,
		title: "Radish Square",
		des: "Square is the main place for $RDS transactions to interact with our upcoming utilities.",
		img: <Feature1 />,
	},
	{
		id: 2,
		title: "Rootpad Service",
		des: "Our launchpad program, designed to grow our creators of our platform and nurture the ecosystem.",
		img: <Feature2 />,
	},
	{
		id: 3,
		title: "Airdrops & Rewards",
		des: "Receive early rewards by holding our in-house Radish NFTs as you watch them grow with the ecosystem",
		img: <Feature3 />,
	},
];

//

const Features = () => (
	<section id="features">
		<Container sx={{ position: "relative", pt: 2, pb: { xs: "164px", md: "280px" } }}>
			<Typography
				data-aos="fade-in"
				data-aos-duration="1000"
				sx={{ fontSize: { xs: 40, md: 48 }, textAlign: "center" }}
				variant="h3"
			>
				Our{" "}
				<Box component="span" color="primary.main">
					Features
				</Box>
			</Typography>
			<Typography
				data-aos="fade-in"
				data-aos-duration="1000"
				variant="body1"
				sx={{
					color: ({ palette }) => (palette?.mode === "light" ? "black.100" : "black.200"),
					fontSize: { xs: "20px", md: "24px" },
					textAlign: "center",
					width: { xs: 1, md: "70%" },
					mx: "auto",
					my: 2,
				}}
			>
				Radish is a friendly platform built on the RADIX DLT with the purpose of streamlining DeFi for
				a new creative economy.
			</Typography>

			<Box sx={{ position: "relative", flexGrow: 1, mt: 8, zIndex: 10 }}>
				<Grid container spacing={{ xs: "25px", lg: "40px" }} columns={{ xs: 2, sm: 8, md: 12 }}>
					{features?.map((feature, i) => (
						<Grid
							key={i}
							data-aos={i === 0 ? "fade-right" : i === 1 ? "fade-up" : "fade-left"}
							data-aos-duration="750"
							item
							xs={2}
							sm={4}
							md={4}
						>
							<Card
								sx={{
									bgcolor: "transparent",
									backgroundImage: ({ palette }) =>
										palette?.mode === "dark"
											? "linear-gradient(180deg, #1D1E22 0%, rgba(42, 46, 58, 0.49) 100%)"
											: "linear-gradient(180deg, #FFFFFF 0%, rgba(235, 237, 242, 0.49) 100%)",
									borderRadius: "24px",
									height: 1,
								}}
							>
								<Box
									sx={{
										display: "flex",
										justifyContent: "center",
										svg: { height: "250px" },
										p: 2,
										pb: 0,
									}}
								>
									{feature.img}
								</Box>

								<CardContent sx={{ px: { xs: "24px", md: "32px" }, pb: { xs: "55px" }, textAlign: "center" }}>
									<Typography gutterBottom variant="h5" component="div" sx={{ fontWeight: 500 }}>
										{feature.title}
									</Typography>

									<Typography
										fontSize={{ xs: "14px", sm: "20px" }}
										sx={{
											color: ({ palette }) =>
												palette?.mode === "light" ? "black.100" : "black.200",
										}}
										lineHeight="30px"
									>
										{feature.des}
									</Typography>
								</CardContent>
							</Card>
						</Grid>
					))}
				</Grid>
			</Box>

			{/***
			 * ====================================
			 * >>> background absolute elements <<<
			 * ====================================
			 ***/}

			<Box
				className="float-right"
				sx={{
					position: "absolute",
					top: "50%",
					left: "36%",
					transform: "translate(-50%, -50%)",
					width: "220px",
					height: "220px",
					borderRadius: "50%",
					bgcolor: "primary.main",
					filter: "blur(150px)",
					zIndex: 1,
				}}
			/>
		</Container>
	</section>
);

export default Features;
