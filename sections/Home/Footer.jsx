import NextLink from "next/link";
import { Box, Container, Grid, Typography } from "@mui/material";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import TelegramIcon from "@mui/icons-material/Telegram";
import LogoIcon from "../../assets/svg/logo-2-white.svg";
import styles from "../../styles/Landing.module.scss";

const Footer = () => (
	<div className={styles["footer-section"]}>
		<Container sx={{ py: 4 }}>
			<Box sx={{ flexGrow: 1 }}>
				<Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }}>
					<Grid item xs={2} sm={4} md={6}>
						<LogoIcon />
						<Typography sx={{ my: 2, width: { xs: 1, md: "260px" } }} variant="body1">
							The perfect soil to cultivate your ideas & unleash your creativity
						</Typography>
					</Grid>

					<Grid item xs={2} sm={4} md={6}>
						<Box
							sx={{
								display: "flex",
								justifyContent: "space-between",
								gap: "100px",
							}}
						>

							<Box>
								<Typography
									variant="h4"
									sx={{
										fontSize: { xs: 20, md: 24 },
										fontWeight: 700,
										mb: 2,
									}}
								>
									Grow with us
								</Typography>
								<Typography sx={{ my: 2, width: { xs: 1, md: "80%" } }} variant="body1">
									Stay close to the community by joining our different social medias
								</Typography>
								<Box sx={{ display: "flex", gap: 2, mt: 3 }}>
									<a href="https://twitter.com/RadishEco" target="_blank" rel="noopener noreferrer">
										<TwitterIcon />
									</a>
									<a href="https://instagram.com/RadishEco" target="_blank" rel="noopener noreferrer">
										<InstagramIcon />
									</a>
									<a href="https://t.me/RadishRoot" target="_blank" rel="noopener noreferrer">
										<TelegramIcon />
									</a>
								</Box>
							</Box>
						</Box>
					</Grid>
				</Grid>
			</Box>
		</Container>
		<Container sx={{ pt: 3, pb: 3 }}>
			<Typography sx={{ fontSize: 16, fontWeight: 400, textAlign: "center" }} variant="body2">
				&copy; 2022, Radish Ecosystem. All Rights Reserved. Powered by Crew Labs
			</Typography>
		</Container>
	</div>
);

export default Footer;
