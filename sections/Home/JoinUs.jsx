import { Box, Button, Typography } from "@mui/material";
import FlowerSvg from "../../assets/svg/flower.svg";
import styles from "../../styles/Landing.module.scss";
import NextLink from "next/link";


const JoinUs = () => (
	<Box component="section" data-aos="fade-up" data-aos-duration="2000" sx={{ px: 3 }}>
		<Box
			className={styles["join-us-container"]}
			sx={{ minHeight: { xs: 385, md: 325 }, gap: "24px", px: { xs: 2.5, sm: 18 } }}
		>
			<Box
				className={styles["top-flower-box"]}
				sx={{
					position: "absolute",
					top: { xs: -38, md: 0 },
					left: { xs: -38, md: 0 },
					transform: { xs: "scale(0.6)", md: "none" },
					zIndex: 0,
				}}
			>
				<FlowerSvg />
			</Box>

			<Typography variant="h1" sx={{ color: "white.main", fontSize: { xs: 28, md: 42 }, fontWeight: 600, maxWidth: "600px" }}>
				Do you want to list your project on our platform?
				</Typography>
			<Button
				variant="contained"
				color="secondary"
				sx={{
					width: { xs: 1, md: "fit-content" },
					height: 58,
					fontSize: { xs: 16, md: 20 },
					color: "white.main",
					px: "24px",
					textTransform: "none", // add this line
				}}
				href="https://t.me/BonaFidePlug"
   				target="_blank"
    			rel="noopener noreferrer"
			>
				Join Us Now!
			</Button>

			<Box
				className={styles["bottom-flower-box"]}
				sx={{
					position: "absolute",
					bottom: { xs: -38, md: 0 },
					right: { xs: -38, md: 0 },
					transform: { xs: "scale(0.6) rotateY(180deg)", md: "rotateY(180deg)" },
				}}
			>
				<FlowerSvg />
			</Box>
		</Box>
	</Box>
);

export default JoinUs;
