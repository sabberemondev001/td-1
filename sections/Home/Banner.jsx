import Image from "next/image";
import NextLink from "next/link";
import { Box, Button, Container, Grid, Typography } from "@mui/material";
import EastIcon from "@mui/icons-material/East";
import { useTheme } from "@mui/material/styles";
import { ThemeUtils } from "../../styles/theme/ThemeComponents";
import styles from "../../styles/Landing.module.scss";

// -----------------------------------------------------------------------

const Banner = () => {
	const { breakpoints, palette } = useTheme();
	const [isModeLight] = ThemeUtils();

	return (
		<Box component="section" id="home" sx={{ position: "relative", bgcolor: "primary.main", mb: 6, pb: 14 }}>
			<Container sx={{ pt: 0 }}>
				<Box sx={{ position: "relative", flexGrow: 1, zIndex: 10 }}>
					<Grid
						container
						spacing={{ xs: 2, md: 0 }}
						columns={{ xs: 2, sm: 8, md: 12 }}
						sx={{ flexDirection: { xs: "column-reverse", md: "row" }, alignItems: "center" }}
					>
						<Grid data-aos="fade-right" data-aos-duration="1000" item xs={2} sm={6} md={7}>
							<Box textAlign={{ xs: "center", md: "inherit" }}>
								<Typography
									variant="h1"
									sx={{
										fontSize: { xs: 40, lg: 50, xl: 64 },
										fontWeight: 600,
										color: "white.main",
										mb: 2,
									}}
								>
									The All-in-One Creative Ecosystem
								</Typography>
								<Typography
									variant="body1"
									sx={{
										fontSize: { xs: 20, md: 22, xl: 24 },
										fontWeight: 400,
										color: "white.main",
										mb: 3,
									}}
								>
									A friendly community dedicated to growing radishes!
								</Typography>

								<Box
									sx={{
										display: "flex",
										gap: 3,
										flexDirection: { xs: "column", md: "row" },
									}}
								>
									<Button
										component={NextLink}
										href="https://www.radishsquare.com"
										endIcon={<EastIcon />}
										color={"secondary"}
										variant="contained"
										sx={{
											fontSize: 20,
											color: "white.main",
											textTransform: "inherit",
											padding: "10px 40px",
										}}
									>
										Explore
									</Button>

									<Button
										component={NextLink}
										href="https://radish-eco.gitbook.io/Whitepaper"
										variant="outlined"
										sx={{
											fontSize: 20,
											padding: "10px 40px",
											textTransform: "inherit",
											color: "white.main",
											borderColor: "white.main",

											":hover": {
												color: "black.main",
												bgcolor: "#ffffffce",
												borderColor: "white.main",
											},
										}}
									>
										Whitepaper
									</Button>
								</Box>
							</Box>
						</Grid>

						<Grid data-aos="fade-left" data-aos-duration="1000" item xs={2} sm={8} md={5}>
							<Box
								className="bounce-animate"
								position="relative"
								width={{ xs: "90vw", sm: "38.5vw" }}
								height={{ xs: "350px", md: "560px" }}
								sx={{ border: 'none' }}
							>
								<Image src="/images/radish-banner.svg" alt="" fill objectFit="contain"   style={{ transform: "scale(1.25)" }}/>
							</Box>
						</Grid>
					</Grid>
				</Box>

				{/***
				 * ====================================
				 * >>> background absolute elements <<<
				 * ====================================
				 ***/}

				<Box
					className="float-right"
					sx={{
						position: "absolute",
						top: "280px",
						left: 0,
						width: { xs: "140px", md: "220px" },
						height: { xs: "140px", md: "220px" },
						borderRadius: "50%",
						bgcolor: "#fff",
						filter: { xs: "blur(80px)", md: "blur(120px)" },
						zIndex: 1,
					}}
				/>

				<Box
					className="float-left"
					sx={{
						position: "absolute",
						top: "500px",
						right: 0,
						width: { xs: "140px", md: "220px" },
						height: { xs: "140px", md: "220px" },
						borderRadius: "50%",
						bgcolor: "#fff",
						filter: { xs: "blur(80px)", md: "blur(120px)" },
						zIndex: 1,
					}}
				/>
			</Container>

			<Box position="absolute" left={0} right={0} bottom={0} maxWidth="100vw">
				<svg
					className={styles["waves"]}
					xmlns="http://www.w3.org/2000/svg"
					xmlnsXlink="http://www.w3.org/1999/xlink"
					viewBox="0 24 150 28"
					preserveAspectRatio="none"
					shapeRendering="auto"
				>
					<defs>
						<path
							id="gentle-wave"
							d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"
						/>
					</defs>
					<g className={styles["parallax"]}>
						<use
							x="48"
							y="0"
							xlinkHref="#gen8tle-wave"
							fill={isModeLight ? "rgba(255,255,255,0.7)" : "rgba(0, 0, 0, 0.7)"}
						/>
						<use
							x="48"
							y="3"
							xlinkHref="#gentle-wave"
							fill={isModeLight ? "rgba(255,255,255,0.5)" : "rgba(0, 0, 0, 0.5)"}
						/>
						<use
							x="48"
							y="5"
							xlinkHref="#gentle-wave"
							fill={isModeLight ? "rgba(255,255,255,0.3)" : "rgba(0, 0, 0, 0.3)"}
						/>
						<use xlinkHref="#gentle-wave" x="48" y="7" fill={isModeLight ? "#F1F2F5" : "#131417"} />
					</g>
				</svg>
			</Box>
		</Box>
	);
};

export default Banner;
