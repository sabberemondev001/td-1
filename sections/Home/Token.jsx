import { Box, Container, Grid, Typography } from "@mui/material";
import TokenSvg from "../../assets/svg/token.svg";

const Token = () => (
	<Box
		id="token"
		component="section"
		sx={{
			position: "relative",
			bgcolor: ({ palette }) => (palette?.mode === "light" ? "white.200" : "black.400"),
			zIndex: 10,
		}}
	>
		<Container sx={{ py: 6 }}>
			<Box sx={{ flexGrow: 1 }}>
				<Grid
					container
					spacing={{ xs: 2, md: 3 }}
					columns={{ xs: 2, sm: 8, md: 12 }}
					sx={{
						alignItems: "center",
					}}
				>
					<Grid data-aos="fade-left" data-aos-duration="1000" item xs={2} sm={4} md={6}>
						<Box sx={{ textAlign: { xs: "center", md: "inherit" } }}>
							<Typography sx={{ fontSize: { xs: 32, md: 48 }, mb: 2 }} variant="h3">
								<Box component="span" color="primary.main">
									$RDS
								</Box>{" "}
								Token
							</Typography>
							<Typography
								sx={{
									fontSize: { xs: 16, md: 24 },
									fontWeight: 400,
									mb: 3,
									color: "black.100",
									width: { xs: 1, md: "85%" },
								}}
							>
								$RDS is a token created on the RADIX DLT used for transactions within the RADISH
								platform and get access to our in-house features.
							</Typography>
						</Box>
					</Grid>
					<Grid data-aos="fade-right" data-aos-duration="1000" item xs={2} sm={4} md={6}>
						<Box sx={{ textAlign: "center", svg: { width: "80%" } }}>
							<TokenSvg />
						</Box>
					</Grid>
				</Grid>
			</Box>
		</Container>
	</Box>
);

export default Token;
