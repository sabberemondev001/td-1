import { useState } from "react";
import NextLink from "next/link";
import {
  Button,
  Container,
  AppBar,
  Box,
  Drawer,
  IconButton,
  useScrollTrigger,
} from "@mui/material";
import PropTypes from "prop-types";
import MenuIcon from "@mui/icons-material/Menu";
import ClearIcon from "@mui/icons-material/Clear";
import LogoIcon from "../../assets/svg/logo-2-white.svg";
import styles from "../../styles/Landing.module.scss";
import {
  MaterialUIButton,
  ThemeUtils,
} from "../../styles/theme/ThemeComponents";

//

const NAV_LIST = ["home", "features", "token", "NFT"];

//

const Navigation = ({ activeSection }) => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const [isModeLight] = ThemeUtils();
  const scrollTriggered = useScrollTrigger({ disableHysteresis: true });

  const handleDrawerToggle = () => {
    setMobileOpen((prev) => !prev);
  };

  const NavLinks = NAV_LIST.map((title, i) => (
    <Box
      key={i}
      component="a"
      className={styles["nav-link"]}
      href={"#" + title}
      onClick={() => {
        if (mobileOpen) handleDrawerToggle();
      }}
      sx={{ fontWeight: activeSection === i ? 700 : 500 }}
    >
      {title === "NFT" ? "NFT" : title}
    </Box>
  ));

  const drawer = (
    <Box
      component="nav"
      sx={{
        position: "relative",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        gap: 4,
        pt: "80px",
        pb: "65px",
        px: 2,
      }}
    >
      <IconButton
        onClick={handleDrawerToggle}
        sx={{ position: "absolute", top: 32, right: 16, color: "white.main" }}
      >
        <ClearIcon />
      </IconButton>

      <Box
        component={NextLink}
        href="/"
        sx={{ "&, svg": { minWidth: "120px", minHeight: "44px", zIndex: 100 } }}
        mb={2}
      >
        <LogoIcon />
      </Box>

      {NavLinks}

      <Box sx={{ display: "flex", alignItems: "center", gap: 2, mt: 2 }}>
        <MaterialUIButton />
      </Box>
    </Box>
  );

  return (
    <Box display="flex">
      <AppBar
        position={"relative"}
        sx={{
          bgcolor: "primary.main",
          backgroundImage: "none",
          boxShadow: 0,
          pt: scrollTriggered ? 2 : 5,
          pb: 0,
        }}
      >
        {/* desktop menu  */}
        <Container>
          <Box className="flex-jus-bet" flexGrow={1} gap={6}>
            <Box
              component={NextLink}
              href="/"
              sx={{
                "&, svg": { minWidth: "120px", minHeight: "44px", zIndex: 100 },
              }}
            >
              <LogoIcon />
            </Box>

            <Box
              component="nav"
              sx={{ display: { xs: "none", lg: "flex" }, alignItems: "center" }}
            >
              {NavLinks}
            </Box>

            <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
              {/* <IconButton
								edge="end"
								onClick={handleDrawerToggle}
								color="info"
								sx={{ display: { lg: "none" }, color: isModeLight ? "black.300" : "white.main" }}
							>
								<MenuIcon />
							</IconButton>

							<MaterialUIButton /> */}
            </Box>
          </Box>
        </Container>
      </AppBar>

      <Drawer
        variant="temporary"
        anchor="top"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{ keepMounted: true }}
        transitionDuration={300}
        sx={{
          fontFamily: "'Montserrat', sans-serif",
          display: { xs: "block", lg: "none" },

          ".MuiPaper-root": {
            ...(isModeLight
              ? { bgcolor: "primary.main" }
              : {
                  backgroundImage:
                    "linear-gradient(180deg, #060614 0%, #2E323E 100%)",
                }),
          },
        }}
      >
        {drawer}
      </Drawer>
    </Box>
  );
};

Navigation.propTypes = {
  window: PropTypes.func,
};

export default Navigation;
