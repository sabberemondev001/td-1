import { useState, useMemo, createContext } from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";

// context state for global app theme mode...
export const ColorModeContext = createContext({ toggleColorMode: () => {} });

// provider component..
export default function MuiCutstomProvider({ children }) {
	const [mode, setMode] = useState("light");

	const colorMode = useMemo(
		() => ({
			toggleColorMode: () => {
				setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
			},
		}),
		[]
	);

	const theme = useMemo(
		() =>
			createTheme({
				palette: {
					mode,
					primary: { main: "#DE345E", dark: "#c7244d" },
					secondary: { main: "#3EB98E", dark: "#37ac83" },
					white: {
						main: "#fff",
						dark: "#f5f5f5",
						100: "#E5E5E5",
						200: "#F1F2F5",
					},
					black: {
						main: "#131417",
						100: "#787A8D",
						200: "#9495A1",
						300: "#282830",
						400: "#1B1D23",
						500: "#272930",
					},
					background: { default: mode === "light" ? "#F1F2F5" : "#131417" },
				},
				typography: { fontFamily: "inherit" },
				components: {
					MuiContainer: {
						defaultProps: { maxWidth: false },
						styleOverrides: {
							root: { maxWidth: "1265px" },
						},
					},
					MuiButton: {
						styleOverrides: {
							root: { "&, &:hover": { boxShadow: "none" } },
						},
					},
					MuiPaper: {
						styleOverrides: {
							root: { "&, &:hover": { boxShadow: "none" } },
						},
					},
				},
			}),
		[mode]
	);

	return (
		<ColorModeContext.Provider value={colorMode}>
			<ThemeProvider theme={theme}>
				<CssBaseline />
				{children}
			</ThemeProvider>
		</ColorModeContext.Provider>
	);
}
