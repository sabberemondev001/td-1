import { useContext } from "react";
import { styled, useTheme } from "@mui/material/styles";
import { Switch, IconButton, useMediaQuery } from "@mui/material";
import { ColorModeContext } from "./MuiThemeProvider";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

// theme utilities..
export const ThemeUtils = () => {
	const { breakpoints, palette } = useTheme();

	const isModeLight = palette.mode === "light";
	const aboveMD = useMediaQuery(breakpoints.up("md"));

	return [isModeLight, aboveMD];
};

// styled components..
const SwitchStyled = styled(Switch)(({ theme: { palette } }) => {
	const isModeLight = palette.mode === "light";

	return {
		width: 70,
		height: 33,
		margin: 10,
		padding: 0,

		"& .MuiSwitch-switchBase": {
			margin: 1,
			padding: 0,

			"&.Mui-checked": {
				color: "#fff",
				transform: "translateX(36px)",

				"& .MuiSwitch-thumb:before": {
					backgroundImage: `url("data:image/svg+xml,%3Csvg width='23' height='23' viewBox='0 0 23 23' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='11.5' cy='11.5' r='11.5' fill='white'/%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M18 12.8087C17.6517 12.8701 17.2933 12.9022 16.9274 12.9022C13.5419 12.9022 10.7974 10.1577 10.7974 6.77214C10.7974 6.15597 10.8883 5.56103 11.0575 5C8.18333 5.5072 6 8.01688 6 11.0365C6 14.422 8.74451 17.1665 12.13 17.1665C14.8994 17.1665 17.2398 15.3301 18 12.8087Z' fill='%23DE345E'/%3E%3C/svg%3E")`,
				},

				"& + .MuiSwitch-track": {
					opacity: 1,
					backgroundColor: "#DE345E",
				},
			},
		},

		"& .MuiSwitch-track": {
			backgroundColor: "white",
			borderRadius: 20,
			opacity: 1,
		},

		"& .MuiSwitch-thumb": {
			width: 23,
			height: 23,
			margin: 4.6,
			backgroundColor: isModeLight ? "#DE345E" : "#001e3c",

			"&:before": {
				content: "''",
				position: "absolute",
				width: "100%",
				height: "100%",
				left: 0,
				top: 0,
				backgroundRepeat: "no-repeat",
				backgroundPosition: "center",
				backgroundImage: `url("data:image/svg+xml,%3Csvg width='23' height='23' viewBox='0 0 23 23' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='11.5' cy='11.5' r='11.5' fill='%23DE345E'/%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M11.4997 5C11.6493 5 11.7705 5.12126 11.7705 5.27083V6.89583C11.7705 7.04541 11.6493 7.16667 11.4997 7.16667C11.3501 7.16667 11.2289 7.04541 11.2289 6.89583V5.27083C11.2289 5.12126 11.3501 5 11.4997 5ZM11.4997 15.8334C11.6493 15.8334 11.7705 15.9547 11.7705 16.1042V17.7292C11.7705 17.8788 11.6493 18.0001 11.4997 18.0001C11.3501 18.0001 11.2289 17.8788 11.2289 17.7292V16.1042C11.2289 15.9547 11.3501 15.8334 11.4997 15.8334ZM5.27083 11.2292C5.12126 11.2292 5 11.3505 5 11.5001C5 11.6497 5.12126 11.7709 5.27083 11.7709H6.89583C7.04541 11.7709 7.16667 11.6497 7.16667 11.5001C7.16667 11.3505 7.04541 11.2292 6.89583 11.2292H5.27083ZM15.8336 11.5001C15.8336 11.3505 15.9549 11.2292 16.1045 11.2292H17.7295C17.879 11.2292 18.0003 11.3505 18.0003 11.5001C18.0003 11.6497 17.879 11.7709 17.7295 11.7709H16.1045C15.9549 11.7709 15.8336 11.6497 15.8336 11.5001ZM7.28658 6.90367C7.18081 6.7979 7.00933 6.7979 6.90356 6.90367C6.79779 7.00944 6.79779 7.18092 6.90356 7.28669L8.05261 8.43574C8.15838 8.5415 8.32986 8.5415 8.43563 8.43574C8.54139 8.32997 8.54139 8.15849 8.43563 8.05272L7.28658 6.90367ZM14.5643 14.5639C14.67 14.4582 14.8415 14.4582 14.9473 14.5639L16.0963 15.713C16.2021 15.8187 16.2021 15.9902 16.0963 16.096C15.9906 16.2018 15.8191 16.2018 15.7133 16.096L14.5643 14.9469C14.4585 14.8412 14.4585 14.6697 14.5643 14.5639ZM15.7134 6.90367C15.8192 6.7979 15.9907 6.7979 16.0964 6.90367C16.2022 7.00944 16.2022 7.18092 16.0964 7.28669L14.9474 8.43574C14.8416 8.5415 14.6701 8.5415 14.5644 8.43574C14.4586 8.32997 14.4586 8.15849 14.5644 8.05272L15.7134 6.90367ZM8.43572 14.5639C8.32996 14.4582 8.15847 14.4582 8.05271 14.5639L6.90366 15.713C6.79789 15.8187 6.79789 15.9902 6.90366 16.096C7.00943 16.2018 7.18091 16.2018 7.28668 16.096L8.43572 14.9469C8.54149 14.8412 8.54149 14.6697 8.43572 14.5639ZM11.5 15.0209C13.2949 15.0209 14.75 13.5658 14.75 11.7709C14.75 9.97598 13.2949 8.52091 11.5 8.52091C9.70507 8.52091 8.25 9.97598 8.25 11.7709C8.25 13.5658 9.70507 15.0209 11.5 15.0209Z' fill='white'/%3E%3C/svg%3E")`,
			},
		},
	};
});

//

export const MaterialUISwitch = () => {
	const { toggleColorMode } = useContext(ColorModeContext);
	const { palette } = useTheme();

	return <SwitchStyled onClick={toggleColorMode} checked={palette.mode === "dark"} />;
};

export const MaterialUIButton = () => {
	const { palette } = useTheme();
	const { toggleColorMode } = useContext(ColorModeContext);

	const isModeLight = palette.mode === "light";

	return (
		<IconButton onClick={toggleColorMode} sx={{ color: isModeLight ? "black.main" : "white.main" }}>
			{isModeLight ? <Brightness4Icon /> : <Brightness7Icon />}
		</IconButton>
	);
};
