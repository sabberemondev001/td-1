import NextLink from "next/link";
import Image from "next/image";
import { Box, Button, Container, Typography } from "@mui/material";
import Grid from "@mui/material/Grid";

const Page404 = () => (
	<Box
		sx={{
			display: "flex",
			justifyContent: "center",
			alignItems: "center",
			minHeight: "100vh",
			bgcolor: "white.main",
		}}
	>
		<Container maxWidth="md">
			<Grid container spacing={2}>
				<Grid xs={6}>
					<Typography variant="h1">404</Typography>
					<Typography variant="h6" mb={2}>
						The page you&apos;re looking for doesn&apos;t exist.
					</Typography>
					<Button variant="contained" component={NextLink} href="/">
						Back Home
					</Button>
				</Grid>

				<Grid xs={6}>
					<Image
						src="https://cdn.pixabay.com/photo/2017/03/09/12/31/error-2129569__340.jpg"
						alt=""
						width={500}
						height={250}
					/>
				</Grid>
			</Grid>
		</Container>
	</Box>
);

export default Page404;
