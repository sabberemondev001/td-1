import Head from "next/head";
import { useEffect, useState } from "react";
import localFont from "@next/font/local";
import { Banner, Features, Footer, JoinUs, Navigation, HomeNft, Token } from "../sections/Home";

//

const montserrat = localFont({
	src: "../assets/fonts/Montserrat/Montserrat-VariableFont_wght.ttf",
});

//

const Home = () => {
	const [activeSection, setActiveSection] = useState(0);

	useEffect(() => {
		const handleScroll = () => {
			document.querySelectorAll("section").forEach((sectionEl, i) => {
				if (sectionEl.offsetTop - window.scrollY < window.innerHeight / 2) {
					setActiveSection(i);
				}
			});
		};

		window.addEventListener("scroll", handleScroll, { passive: true });
		return () => window.removeEventListener("scroll", handleScroll);
	}, []);

	return (
		<>
			<Head>
				<title>Radish - The Friendliest Community of Radishes</title>
			</Head>

			<main className={montserrat.className}>
				<Navigation activeSection={activeSection} />
				<Banner />
				<Features />
				<Token />
				<HomeNft />
				<JoinUs />
				<Footer />
			</main>
		</>
	);
};

export default Home;
