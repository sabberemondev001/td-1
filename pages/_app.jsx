import { useEffect, useState } from "react";
import localFont from "@next/font/local";
import Aos from "aos";
import DomLoader from "../layout/DomLoader";
import MuiCutstomProvider from "../styles/theme/MuiThemeProvider";
import "aos/dist/aos.css";
import "../styles/globals.scss";

//

const poppins = localFont({
	src: [
		{
			path: "../assets/fonts/Poppins/Poppins-Light.ttf",
			weight: "300",
			style: "normal",
		},
		{
			path: "../assets/fonts/Poppins/Poppins-Regular.ttf",
			weight: "400",
			style: "normal",
		},
		{
			path: "../assets/fonts/Poppins/Poppins-Medium.ttf",
			weight: "500",
			style: "normal",
		},
		{
			path: "../assets/fonts/Poppins/Poppins-SemiBold.ttf",
			weight: "600",
			style: "normal",
		},
		{
			path: "../assets/fonts/Poppins/Poppins-Bold.ttf",
			weight: "700",
			style: "normal",
		},
	],
});

//

function MyApp({ Component, pageProps }) {
	const [isDomLoading, setDomLoading] = useState(true);

	useEffect(() => {
		setInterval(() => {
			setDomLoading(false);
		}, 300);

		Aos.init({
			easing: "ease-out-cubic",
			offset: 50,
			once: true,
		});
	}, []);

	return (
		<MuiCutstomProvider>
			{isDomLoading ? (
				<DomLoader />
			) : (
				<main className={poppins.className}>
					<Component {...pageProps} />
				</main>
			)}
		</MuiCutstomProvider>
	);
}

export default MyApp;
