/** @type {import('next').NextConfig} */
module.exports = {
	reactStrictMode: true,
	eslint: { ignoreDuringBuilds: true },
	webpack(config) {
		config.module.rules.push({
			test: /\.svg$/i,
			issuer: /\.[jt]sx?$/,
			use: ["@svgr/webpack"],
		});

		return config;
	},
	images: {
		remotePatterns: [
			{ protocol: "https", hostname: "cloudflare-ipfs.com" },
			{ protocol: "https", hostname: "loremflickr.com" },
			{ protocol: "https", hostname: "cdn.pixabay.com" },
			{ protocol: "https", hostname: "buffwild.b-cdn.net" },
		],
	},
};
