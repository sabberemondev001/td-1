export const elipsisMiddle = (str, firstIndex = 3, lastIndex = -8) => {
	return str ? (str.length > 12 ? `${str.slice(0, firstIndex)}...${str.slice(lastIndex)}` : str) : null;
};
