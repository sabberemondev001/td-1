export const ApiSample = [
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/1.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 1,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 19",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "Red, Yellow",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/2.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 2,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/3.png",
		ownerWallet: "rdx1qspz966874ewcr36fvu4dve9rl376vedez033sla5w2thqa4vmzvtpcqq09sf",
		title: null,
		uniqueId: 3,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 13",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "Chromed Out",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Animal Horns",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/4.png",
		ownerWallet: "rdx1qspm3lavdmzzzhxlujv6ndhemyqc77003rhunatjstvnnx0z9s6uunszdvs90",
		title: null,
		uniqueId: 4,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/5.png",
		ownerWallet: "rdx1qspph0gh69mlxmmv3dddtyy64lstyefkjzaz29esq87348p029xahvq7ylhwx",
		title: null,
		uniqueId: 5,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 23",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Animal Horns",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Weapons",
			},
		],
	},
	{
		date_listed: "1261-10-17 01:25:08.874934",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/6.png",
		ownerWallet: "rdx1qspzn8e9jskne6flr43l0nslaq8y45w7j2lx3p0tqt4c84kc6t55m0smgusgz",
		title: null,
		uniqueId: 6,
		"project name": "Buff Wild",
		type: "Art",
		price: 2700.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 17",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Betrayer",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/7.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 7,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 13",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "The Elders",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/8.png",
		ownerWallet: "rdx1qsp92rj8aknl2whdgawdm683ha2dpp5z5strawscvk9a5vhg9wvl3qgxqfu8v",
		title: null,
		uniqueId: 8,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 23",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "The Demi Buff",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "Black",
			},
			{
				trait_type: "Mouth",
				value: "Prehistoric",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/9.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 9,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 4",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/10.png",
		ownerWallet: "rdx1qspm3lavdmzzzhxlujv6ndhemyqc77003rhunatjstvnnx0z9s6uunszdvs90",
		title: null,
		uniqueId: 10,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/11.png",
		ownerWallet: "rdx1qspxyzsmfgk3h29zrwcxkrywgwquuvqyce5gs2dpafree242dv0zxmgdw85wl",
		title: null,
		uniqueId: 11,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 8",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/12.png",
		ownerWallet: "rdx1qsp9nqq3mt5x3hl8jsttssserws9hj40hjaul6fas3qckg0enprugjcewxlte",
		title: null,
		uniqueId: 12,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 2",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: "1305-10-17 01:25:08.579866",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/13.png",
		ownerWallet: "rdx1qsp8g30n83z7rje26qe3am52pqr9dhq7s3qja0ys45favkqhlw487ccwm5yu4",
		title: null,
		uniqueId: 13,
		"project name": "Buff Wild",
		type: "Art",
		price: 750.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Weapons",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/14.png",
		ownerWallet: "rdx1qsptxddvu4z47etv5gqm823kv7xywg5m4smz63jy9jlqlgtrvzdex3qha5dka",
		title: null,
		uniqueId: 14,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 5",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/15.png",
		ownerWallet: "rdx1qspdzrds6skway4r04c9y8xhn64n0xa0rum80vjt98dctn8nlhcywwcp03uml",
		title: null,
		uniqueId: 15,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/16.png",
		ownerWallet: "rdx1qspmxf5d9wt6dwgcftdy3pph3wscgvj0lw8lqsmpvsj2978ehyekcfcqas8d6",
		title: null,
		uniqueId: 16,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 6",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/17.png",
		ownerWallet: "rdx1qspz966874ewcr36fvu4dve9rl376vedez033sla5w2thqa4vmzvtpcqq09sf",
		title: null,
		uniqueId: 17,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 21",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "White Angel",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/18.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 18,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 14",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Weapons",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/19.png",
		ownerWallet: "rdx1qspe8mhy7kv9qhsr2ufefqqu7rdmde8mkl0aqxmupht7a5f8tqcaf2svmgq7a",
		title: null,
		uniqueId: 19,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 11",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Animal Horns",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/20.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 20,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 8",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/21.png",
		ownerWallet: "rdx1qsp9l99l9puul4myy7y8mcxsnn50qr289n6nzv38khqnq82k6he5f4q8wkc6w",
		title: null,
		uniqueId: 21,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 5",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/22.png",
		ownerWallet: "rdx1qsp0gw7eajly5zsztv4s89jaec5vd5vu08t8j980wyj489jkpflylrs4449kn",
		title: null,
		uniqueId: 22,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 16",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "Undead",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: "1954-10-17 01:25:03.770849",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/23.png",
		ownerWallet: "rdx1qspcu3wqnh49q65qz9gzxa467th89307352upejg4j3gtl55sjn48fc99d8j2",
		title: null,
		uniqueId: 23,
		"project name": "Buff Wild",
		type: "Art",
		price: 180.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 6",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/24.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 24,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 13",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "Blue",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/25.png",
		ownerWallet: "rdx1qspknyachak4n9th2ttvjgw7k72a9qtedl55758rms29w98syt7vf3q6nd0pj",
		title: null,
		uniqueId: 25,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/26.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 26,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 34",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "Brads And Chads",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Betrayer",
			},
			{
				trait_type: "Fur",
				value: "The Elders",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "Red",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Ink",
			},
		],
	},
	{
		date_listed: "1262-10-17 01:25:08.867932",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/27.png",
		ownerWallet: "rdx1qspzn8e9jskne6flr43l0nslaq8y45w7j2lx3p0tqt4c84kc6t55m0smgusgz",
		title: null,
		uniqueId: 27,
		"project name": "Buff Wild",
		type: "Art",
		price: 3300.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 19",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "Gentleman",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/28.png",
		ownerWallet: "rdx1qspte8g25zw43v2pzywg4nz4ksfsvdcdv5k9kvwrejltk5a2xc8u9vgjpmhmd",
		title: null,
		uniqueId: 28,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Animal Horns",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/29.png",
		ownerWallet: "rdx1qsp0kktq49w35mh55spqj7kayeufrp6njzwfn7488uc3v60acjmnlgc0x4vmf",
		title: null,
		uniqueId: 29,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 11",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "Rasta",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/30.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 30,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: "1396-10-17 01:25:07.959727",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/31.png",
		ownerWallet: "rdx1qspky24p8azrlgsmdapjmvt6s4v275reufs6j8x3ssknehyl88sjxjq6a9tff",
		title: null,
		uniqueId: 31,
		"project name": "Buff Wild",
		type: "Art",
		price: 777.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: "1462-10-17 01:25:07.464616",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/32.png",
		ownerWallet: "rdx1qspwds5yqp949aa5yq22kcyuu6nl5edkevqtkfz4mq76sqrm0v45n3ckcwzyf",
		title: null,
		uniqueId: 32,
		"project name": "Buff Wild",
		type: "Art",
		price: 400.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 12",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/33.png",
		ownerWallet: "rdx1qspxyzsmfgk3h29zrwcxkrywgwquuvqyce5gs2dpafree242dv0zxmgdw85wl",
		title: null,
		uniqueId: 33,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "Blue",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/34.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 34,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 14",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Halo",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/35.png",
		ownerWallet: "rdx1qspepevu9hcgshzt6wjarrgrhqp3w4sm8n0u2qkdmkyjs7hhqj6ejqsa0ysu3",
		title: null,
		uniqueId: 35,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 16",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Prehistoric",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/36.png",
		ownerWallet: "rdx1qspcpgg5rpxd5pmrjtawvvu26fjuy7ans4ed8kcdr9yj2suul8jrdrgvvs0wz",
		title: null,
		uniqueId: 36,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 12",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "Blue",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/37.png",
		ownerWallet: "rdx1qsphslf9tl49whpzu7txq6rarkagh8wdxqlwjlsx6pdug2lu692ytcgvestyq",
		title: null,
		uniqueId: 37,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Halo",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/38.png",
		ownerWallet: "rdx1qspgcq8w98est9e5ngs90pn26r69838ylkwx8cw8h3evccwat8gfj2gg8sedy",
		title: null,
		uniqueId: 38,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/39.png",
		ownerWallet: "rdx1qsprdtvgt7kghjdvpy86prw5k0l8t2fvzgldjcuprukly57hm7lgzas4p9fll",
		title: null,
		uniqueId: 39,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/40.png",
		ownerWallet: "rdx1qsp8xmre42lm7e4lawvrkq4vfz08jdr8umqlsp2604r935e5fk0x86c84csra",
		title: null,
		uniqueId: 40,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 18",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Prehistoric",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Weapons",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/41.png",
		ownerWallet: "rdx1qsp0xhkzleyhf8zx2g6czgydj0q097whrl5ej7ldj6fz8mx2ljw5hscm9h8ka",
		title: null,
		uniqueId: 41,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "Red",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/42.png",
		ownerWallet: "rdx1qspj49zjygwp3zeqtywutz5jyv5pyfpztm5mqt5w9sj7qjtkfgzzfngswj5j5",
		title: null,
		uniqueId: 42,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 6",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: "1492-10-17 01:25:07.241566",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/43.png",
		ownerWallet: "rdx1qsph4cfd8ayjeh3k6q868kxqnutdveuh28tmtew6ns5a856l3gwjgjcw8mw6g",
		title: null,
		uniqueId: 43,
		"project name": "Buff Wild",
		type: "Art",
		price: 250.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 8",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/44.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 44,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 5",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: "1454-10-17 01:25:07.524630",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/45.png",
		ownerWallet: "rdx1qsp9daks9sjy83azs0d6svmqhx9390nej7qydymkqc96ng3yw8aha2cmgdhc9",
		title: null,
		uniqueId: 45,
		"project name": "Buff Wild",
		type: "Art",
		price: 650.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/46.png",
		ownerWallet: "rdx1qspknyachak4n9th2ttvjgw7k72a9qtedl55758rms29w98syt7vf3q6nd0pj",
		title: null,
		uniqueId: 46,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 4",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/47.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 47,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 15",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "Undead",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/48.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 48,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Halo",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/49.png",
		ownerWallet: "rdx1qspnm6gt0a4aa8mngf6d0lfg6du7g54xp60vwxg68dtklqg0ednpggc6h6zsk",
		title: null,
		uniqueId: 49,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 3",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/50.png",
		ownerWallet: "rdx1qsplxvk5pmq3gtmpztskju0wz03j336kgexqjf7mu236m3l8vwackgsqee0jx",
		title: null,
		uniqueId: 50,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 16",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "Purple",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/51.png",
		ownerWallet: "rdx1qspr73f5qx64cmrtecg7y5tkw7guf5h2rc2n49ddgkhz2fvtsawnm0smwncle",
		title: null,
		uniqueId: 51,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/52.png",
		ownerWallet: "rdx1qspl3lpjkn2zkarnq3jfhlrup0ffuyepclym3xn3uqajqek744ljzxgvy2fx4",
		title: null,
		uniqueId: 52,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/53.png",
		ownerWallet: "rdx1qspjqyhw7s7j780l30jtawnaxqx2cx2nyr8sf0y73mva7kllrg062xsurk79m",
		title: null,
		uniqueId: 53,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 11",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "Yes",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/54.png",
		ownerWallet: "rdx1qspzwgrdpuxj2zt7sexlnapdax7f2lf8k3lgh8ve5wztdvec64pwe8stxctpn",
		title: null,
		uniqueId: 54,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 12",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Betrayer",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/55.png",
		ownerWallet: "rdx1qspgcq8w98est9e5ngs90pn26r69838ylkwx8cw8h3evccwat8gfj2gg8sedy",
		title: null,
		uniqueId: 55,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 20",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "The Demi Buff",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/56.png",
		ownerWallet: "rdx1qsp9lam487yuh6pzvcjlg2cxskz9wx6nk33lspk2vykf9aj85gcdkgclruu3n",
		title: null,
		uniqueId: 56,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Cool Guy",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/57.png",
		ownerWallet: "rdx1qspjqyhw7s7j780l30jtawnaxqx2cx2nyr8sf0y73mva7kllrg062xsurk79m",
		title: null,
		uniqueId: 57,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Designer",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Bull Rings",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/58.png",
		ownerWallet: "rdx1qsp9f7xjady7k4acrhn9hqadk65c3kyucwmxmz53e0xhmepfatg4nzsvfcjpu",
		title: null,
		uniqueId: 58,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 13",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Halo",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/59.png",
		ownerWallet: "rdx1qsp4qzcwdhurmfsh384yecc9tfqk9xmm42qsjd7zvysknqxty8k608cjz3yqh",
		title: null,
		uniqueId: 59,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 38",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "Demigod",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Devilish",
			},
			{
				trait_type: "Horns",
				value: "Halo",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Bane Veins",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/60.png",
		ownerWallet: "rdx1qspcp55ffggesr7yjenw0zcshjw322tghnf7lp9qrs5xljwxpfau78gursmml",
		title: null,
		uniqueId: 60,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 18",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Beams",
			},
			{
				trait_type: "Fur",
				value: "White Angel",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/61.png",
		ownerWallet: "rdx1qsp328xugrxvazuzxcflys5g8ccndyy95h7axvzfyp6p03e3q87j5ps2l92ls",
		title: null,
		uniqueId: 61,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 13",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard Plus",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Prehistoric",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/62.png",
		ownerWallet: "rdx1qspepevu9hcgshzt6wjarrgrhqp3w4sm8n0u2qkdmkyjs7hhqj6ejqsa0ysu3",
		title: null,
		uniqueId: 62,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 6",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "Black",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: "1130-10-17 01:25:09.767134",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/63.png",
		ownerWallet: "rdx1qsp6wwvr5yzfrvpefghkhf2c0nh8mcya27ze8mlttgxl2vu59pf8rtcjlkqqf",
		title: null,
		uniqueId: 63,
		"project name": "Buff Wild",
		type: "Art",
		price: 300.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Standard",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "Pink",
			},
			{
				trait_type: "Mouth",
				value: "KEWL",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/64.png",
		ownerWallet: "rdx1qsp5095ch44yufrpkukq3qevf9qj4vad25x5en5m2gvqvsqrs507tyg9akgxh",
		title: null,
		uniqueId: 64,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 10",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Animal Horns",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/65.png",
		ownerWallet: "rdx1qsp4mnq55f7qvnlnsp5e4drzxsw9ajeynwwqr4cuhycejrvjz9s0c5ggzec3h",
		title: null,
		uniqueId: 65,
		"project name": "Buff Wild",
		type: "Art",
		price: null,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 7",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Variant",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Middle Class Fancy",
			},
		],
	},
	{
		date_listed: "1253-10-17 01:25:08.926945",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/66.png",
		ownerWallet: "rdx1qsp4qzcwdhurmfsh384yecc9tfqk9xmm42qsjd7zvysknqxty8k608cjz3yqh",
		title: null,
		uniqueId: 66,
		"project name": "Buff Wild",
		type: "Art",
		price: 120.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Atmospheric",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Impressionable",
			},
			{
				trait_type: "Fur",
				value: "The Risen",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Baby Buff",
			},
		],
	},
	{
		date_listed: null,
		forSale: false,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/67.png",
		ownerWallet: "Locked",
		title: null,
		uniqueId: 67,
		"project name": "Buff Wild",
		type: "Art",
		price: 100,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 9",
		attributes: [
			{
				trait_type: "Background",
				value: "Colors",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Battle Ridden",
			},
			{
				trait_type: "Fur",
				value: "Animal",
			},
			{
				trait_type: "Horns",
				value: "Standard",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Heated",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Standard",
			},
		],
	},
	{
		date_listed: "1265-10-17 01:25:08.849928",
		forSale: true,
		img_url: "https://buffwild.b-cdn.net/Buff%20Wild%20Collection%20-%20Metadata/images/68.png",
		ownerWallet: "rdx1qspky24p8azrlgsmdapjmvt6s4v275reufs6j8x3ssknehyl88sjxjq6a9tff",
		title: null,
		uniqueId: 68,
		"project name": "Buff Wild",
		type: "Art",
		price: 800.0,
		currency: "XRD",
		"buy type": "Random",
		"recent activity time": "",
		"recent activity type": "",
		"price history": [],
		description: "Total Buff Score: 8",
		attributes: [
			{
				trait_type: "Background",
				value: "Dimensions",
			},
			{
				trait_type: "Collection(s)",
				value: "No",
			},
			{
				trait_type: "Double Baby Buff",
				value: "No",
			},
			{
				trait_type: "Eyes",
				value: "Eyewear",
			},
			{
				trait_type: "Fur",
				value: "Standard",
			},
			{
				trait_type: "Horns",
				value: "Baby Buff",
			},
			{
				trait_type: "Matching",
				value: "No",
			},
			{
				trait_type: "Mouth",
				value: "Standard",
			},
			{
				trait_type: "Smoking",
				value: "No",
			},
			{
				trait_type: "Swag",
				value: "Premium Swag",
			},
		],
	},
];
