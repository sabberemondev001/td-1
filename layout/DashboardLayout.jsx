import { useState } from "react";
import Image from "next/image";
import NextLink from "next/link";
import { useRouter } from "next/router";
import {
	Divider,
	FormControlLabel,
	FormGroup,
	AppBar,
	Box,
	Button,
	Drawer,
	IconButton,
	ListItem,
	ListItemIcon,
	ListItemText,
	Toolbar,
	Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import ClearIcon from "@mui/icons-material/Clear";
import { Modal404 } from "../components";
import { MaterialUISwitch, ThemeUtils } from "../styles/theme/ThemeComponents";
import DashboardIcon from "../assets/svg/dashboard-icon.svg";
import FaqIcon from "../assets/svg/faq-icon.svg";
import RadefiIcon from "../assets/svg/radefi-icon.svg";
import RootpadIcon from "../assets/svg/rootpad-icon.svg";
import SettingIcon from "../assets/svg/setting-icon.svg";
import SquareIcon from "../assets/svg/square-icon.svg";
import NotificationIcon from "../assets/svg/notification-icon.svg";
import styles from "../styles/DashboardLayout.module.scss";

//

const DRAWER_WIDTH = 240;

//

const Dashboard = ({ children }) => {
	const [mobileOpen, setMobileOpen] = useState(false);
	const [mobileSearchOpen, setMobileSearchOpen] = useState(false);
	const [is404ModalOpen, set404ModalOpen] = useState(false);

	const { asPath, push } = useRouter();
	const [isModeLight, aboveMD] = ThemeUtils();

	const handleDrawerToggle = () => {
		setMobileOpen((prev) => !prev);
	};

	const handleMobileSearchToggle = () => {
		setMobileSearchOpen((prev) => !prev);
	};

	const NavigationItem = ({ href, icon, name, under404 }) => {
		const isActive = href !== "" ? asPath.startsWith("/dashboard" + href) : asPath.split("/")?.length <= 2;

		return (
			<NextLink
				href={under404 ? "#" : "/dashboard" + href}
				passHref
				onClick={() => (under404 ? set404ModalOpen(true) : null)}
			>
				<ListItem
					className={`${styles["list-item"]} ${isActive ? styles["nav-selected"] : ""}`}
					sx={{ opacity: isModeLight || isActive ? 1 : 0.6 }}
					button
				>
					<ListItemIcon>{icon}</ListItemIcon>
					<ListItemText primary={name} />
				</ListItem>
			</NextLink>
		);
	};

	const drawer = (
		<>
			<Toolbar
				sx={{
					display: "flex",
					textAlign: "center",
					justifyContent: "center",
					height: { xs: "35%", lg: "30%" },
					pt: "30px",
					pb: "48px",
				}}
			>
				<Box component={NextLink} href="/" position="relative" width="70%" height={150} mt={6} mb={3}>
					<Image src="/images/main-logo.png" alt="" objectFit="contain" fill />
				</Box>
			</Toolbar>

			<Box
				component="nav"
				sx={{
					fontFamily: "'Poppins', sans-serif",
					height: { xs: "65%", lg: "70%" },
					display: "flex",
					flexDirection: "column",
					justifyContent: "space-between",
					gap: "20px",
					pb: 3.4,
				}}
			>
				<Box>
					<NavigationItem href="" name="Dashboard" icon={<DashboardIcon />} />
					<NavigationItem href="/radish-square" name="Radish Square" icon={<SquareIcon />} />
					<NavigationItem name="RaDefi" icon={<RadefiIcon />} under404 />
					<NavigationItem name="Rootpad" icon={<RootpadIcon />} under404 />
				</Box>

				<Divider sx={{ borderColor: isModeLight ? "#F05E82" : "#32324B" }} />

				<Box>
					<NavigationItem name="Settings" icon={<SettingIcon />} under404 />
					<NavigationItem name="FAQ" icon={<FaqIcon />} under404 />
				</Box>

				<FormGroup sx={{ mx: "auto" }}>
					<FormControlLabel control={<MaterialUISwitch sx={{ m: 1 }} defaultChecked />} />
				</FormGroup>
			</Box>
		</>
	);

	return (
		<Box>
			<AppBar
				position="static"
				elevation={0}
				sx={{
					width: { lg: `calc(100% - ${DRAWER_WIDTH}px)` },
					bgcolor: "transparent",
					ml: { lg: `${DRAWER_WIDTH}px` },
				}}
			>
				<Toolbar
					sx={{
						display: "flex",
						justifyContent: "space-between",
						gap: 4,
						bgcolor: "transparent",
						boxShadow: 0,
						px: { xs: "24px", md: "41px" },
						pt: { xs: "36px", md: "44px" },
						pb: "22px",
					}}
				>
					<Typography
						variant="h3"
						noWrap
						sx={{
							color: isModeLight ? "black.main" : "white.main",
							fontSize: { xs: 20, md: 32 },
							fontWeight: 600,
						}}
					>
						{asPath === "/dashboard"
							? "Dashboard"
							: asPath === "/dashboard/radish-square"
							? "Radish Square"
							: asPath === "/dashboard/radefi"
							? "RaDefi"
							: asPath === "/dashboard/rootpad"
							? "Rootpad"
							: asPath === "/dashboard/setting"
							? "Settings"
							: asPath === "/dashboard/faq"
							? "FAQ"
							: "Dashboard"}
					</Typography>

					<Box sx={{ display: "flex", alignItems: "center", gap: { xs: "13px", sm: 3, lg: 4 } }}>
						<Box
							component="form"
							onSubmit={(e) => {
								e.preventDefault();
								push("/dashboard/collections");
							}}
							className={styles["search-box"]}
							onClick={() => (aboveMD ? null : handleMobileSearchToggle())}
							sx={{
								minWidth: { md: "240px", lg: "345px" },
								maxWidth: { md: "240px", lg: "345px" },
								bgcolor: { md: isModeLight ? "transparent" : "black.500" },
								border: { xs: "none", md: isModeLight ? "1px solid #CDCFE0" : "none" },
								pl: { md: "20px" },

								input: {
									display: { xs: "none", md: "block" },
									color: isModeLight ? "black.main" : "white.main",
								},

								svg: { color: "secondary.main", width: { xs: "22px", sm: "26px", md: "22px" } },
							}}
						>
							{aboveMD ? (
								<SearchIcon />
							) : (
								<IconButton>
									<SearchIcon />
								</IconButton>
							)}

							<input type="search" placeholder="Search..." />
						</Box>

						<IconButton
							className={styles["notification"]}
							sx={{
								width: { xs: 32, sm: 56 },
								height: { xs: 32, sm: 56 },
								bgcolor: isModeLight ? "white.main" : "black.500",
								svg: { transform: { xs: "scale(0.7)", sm: "none" } },
								path: { fill: isModeLight ? "#000" : "#fff" },
								"::after": { width: { xs: "8px", sm: "12px" }, height: { xs: "8px", sm: "12px" } },
								borderRadius: 9999,
								p: 0,
							}}
						>
							<NotificationIcon />
						</IconButton>

						<Button
							component={NextLink}
							href="/dashboard/profile"
							sx={{
								display: "flex",
								alignItems: "center",
								gap: "18px",
								cursor: "pointer",
								borderRadius: "10px",
								textAlign: "left",
							}}
						>
							<Box
								sx={{
									position: "relative",
									width: { xs: 32, sm: 56 },
									height: { xs: 32, sm: 56 },
									borderRadius: 9999,
									overflow: "hidden",
								}}
							>
								<Image src="/images/user.png" alt="" fill />
							</Box>

							<Box sx={{ display: { xs: "none", md: "block" } }}>
								<Typography
									variant="h6"
									sx={{
										color: isModeLight ? "black.300" : "white.main",
										fontSize: 16,
										fontWeight: 500,
									}}
								>
									Immanuel Cessar
								</Typography>
								<Typography
									sx={{
										color: isModeLight ? "black.100" : "black.200",
										fontSize: 14,
										fontWeight: 400,
									}}
								>
									@cessar
								</Typography>
							</Box>
						</Button>

						<IconButton
							color="inherit"
							aria-label="open drawer"
							edge="start"
							onClick={handleDrawerToggle}
							sx={{ display: { lg: "none" }, color: isModeLight ? "black.300" : "white.main" }}
						>
							<MenuIcon />
						</IconButton>
					</Box>
				</Toolbar>
			</AppBar>

			<Box sx={{ width: { lg: DRAWER_WIDTH }, flexShrink: { lg: 0 } }} aria-label="mobile sidebar">
				{/***
				 * ========================
				 * mobile drawer sidebar
				 * ========================
				 **/}

				<Drawer
					variant="temporary"
					open={mobileOpen}
					onClose={handleDrawerToggle}
					ModalProps={{ keepMounted: true }}
					sx={{
						display: { xs: "block", lg: "none" },

						"& .MuiDrawer-paper": {
							width: DRAWER_WIDTH,
							bgcolor: isModeLight ? "primary.main" : "",
							backgroundImage: isModeLight ? "" : "linear-gradient(180deg, #060614 0%, #2E323E 100%)",
							"&::-webkit-scrollbar": { display: "none" },
						},
					}}
				>
					{drawer}
				</Drawer>

				{/***
				 * ========================
				 * desktop sidebar
				 * ========================
				 **/}

				<Drawer
					variant="permanent"
					open
					sx={{
						display: { xs: "none", lg: "block" },

						"& .MuiDrawer-paper": {
							width: DRAWER_WIDTH,
							bgcolor: isModeLight ? "primary.main" : "",
							backgroundImage: isModeLight ? "" : "linear-gradient(180deg, #060614 0%, #2E323E 100%)",
							"&::-webkit-scrollbar": { display: "none" },
						},
					}}
				>
					{drawer}
				</Drawer>

				{/***
				 * ========================
				 * mobile search-bar
				 * ========================
				 **/}

				<Drawer
					anchor="top"
					variant="temporary"
					open={mobileSearchOpen}
					onClose={handleMobileSearchToggle}
					ModalProps={{ keepMounted: true }}
					sx={{
						display: { xs: "block", md: "none" },

						"& .MuiDrawer-paper": {
							bgcolor: isModeLight ? "primary.main" : "black.main",
							backgroundImage: "none",
						},
					}}
				>
					<Box height={144} position="relative" className="flex-all-cen">
						<IconButton
							onClick={handleMobileSearchToggle}
							sx={{
								position: "absolute",
								top: 12,
								right: 16,
								color: "white.main",
								svg: { width: 12, height: 12 },
							}}
						>
							<ClearIcon />
						</IconButton>

						<Box
							component="form"
							className={styles["search-box"]}
							onSubmit={(e) => {
								console.log(e);
								e.preventDefault();
								push("/dashboard/collections");
							}}
							sx={{
								width: { xs: "100%", sm: "80%" },
								bgcolor: isModeLight ? "white.main" : "black.500",
								border: isModeLight ? "1px solid #CDCFE0" : "none",
								pl: "20px",
								mx: 3,
								svg: { color: "secondary.main", width: { xs: "20px", sm: "26px" } },
							}}
						>
							<SearchIcon />
							<input type="search" placeholder="Search..." />
						</Box>
					</Box>
				</Drawer>
			</Box>

			{/***
			 * ========================
			 * main content
			 * ========================
			 **/}
			<Box
				component="main"
				sx={{
					ml: { lg: `${DRAWER_WIDTH}px` },
					flexGrow: 1,
					px: { xs: "24px", md: "41px" },
					pt: { sm: "14px" },
					pb: "12vh",
				}}
			>
				{children}
			</Box>

			<Modal404 open={is404ModalOpen} setOpen={set404ModalOpen} />
		</Box>
	);
};

export default Dashboard;
