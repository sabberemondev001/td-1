import { CircularProgress, Box } from "@mui/material";
import StrawberryIcon from "../assets/svg/strawberry-logo.svg";

//

const DomLoader = () => (
	<Box height="100vh" className="flex-all-cen">
		<Box position="relative" display="inline-flex">
			<CircularProgress size={120} thickness={2} />

			<Box
				className="flex-all-cen"
				position="absolute"
				top={0}
				left={0}
				right={0}
				bottom={0}
				sx={{ svg: { transform: "scale(1.2)" } }}
			>
				<StrawberryIcon />
			</Box>
		</Box>
	</Box>
);

export default DomLoader;
